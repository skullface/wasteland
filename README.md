Wasteland
=========

A semantic [Twine][1] Story Format focused on readability.

[1]: <http://twinery.org/>

Installation
------------

Download the .zip & unzip it into your Twine/targets directory. This is where
your Sugar Cane and Jonah folders already live. Wasteland needs its own folder
and the header.html file.



History
-------

0.0.1: initial exploration of Twine Story Format, very basic typography
treatment.
